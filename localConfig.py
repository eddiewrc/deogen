#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  localConfig.py
#  
#  Copyright 2015 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#THIRD PARTY APPLICATIONS PATHS  #add your configuration here!
PROVEAN_SH="./thirdParty/provean-1.1.5/bin/provean.sh"
JH_BIN="./thirdParty/hmmer-3.1b2-linux-intel-x86_64/binaries/jackhmmer"
JH_DATABASE="./thirdParty/jhDB/uniref100.fasta"
CORES=3
WORKING_DIR=DOWNLOAD_DIR="./scratchDIDAsnvs/"
TMP_DIR = "./proveanTMP/"
SAVE_SUPPORTING = True
JH_MSA_FOLDER = "./jhMSAs/"#"./andreaJH1iter0.1evalue/"#"./jhMSAs/"
PRECOMPUTED_PROVEAN_PREDS_FOLDER= "./thirdParty/proveanPredictions/"
RESULTS_DIR="./DEOGEN_RESULTS/"


# ALGORITHM SETTINGS (leave unchanged unless you REALLY know what you're doing!)
SI=0.85
COV=0.80
MSA_FILT_SUFFIX=".filt85SI80COV"
MAX_ITER=3
RETRY=5
JH_EVALUE=0.1
JH_ITER=1 
REFORMAT="./sources/reformat.pl "
TRIM="./sources/trim.py "


def main():
	print "Please don't run me! Just set the proper paths here!\nRun deogen-snv.py instead!"
	return 0

if __name__ == '__main__':
	main()

