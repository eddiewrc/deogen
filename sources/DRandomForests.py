#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


class DRandomForest:
	
	def __init__(self,n_estimators=100, criterion='gini', n_jobs=4, min_samples_leaf=100, min_samples_split=1):
		from sklearn.ensemble import RandomForestClassifier
		#self.ml = RandomForestClassifier(n_estimators=100, min_samples_leaf=500, n_jobs=1)
		self.ml = RandomForestClassifier(n_estimators=n_estimators, criterion=criterion, min_samples_leaf=min_samples_leaf, n_jobs=n_jobs, min_samples_split=min_samples_split)

	def fit(self, X, Y):
		self.ml.fit(X, Y)

	def predict(self, X):
		return self.ml.predict(X)

	def decision_function(self, X):
		res = self.ml.predict_proba(X)
		return res[:,1]
		
	def saveModel(self):
		import cPickle
		cPickle.dump(self.ml, open("DRandomForest.model.cPickle","w"))

def main():
	
	return 0

if __name__ == '__main__':
	main()

