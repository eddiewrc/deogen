#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import re, math
aleph = {'ala':"A", 'arg':"R", 'asn':"N", 'asp':"D", 'cys':"C", 'glu':"E", 'gln':"Q", 'gly':"G", 'his':"H", 'ile':"I", 'leu':"L", 'lys':"K", 'met':"M", 'phe':"F", 'pro':"P", 'ser':"S", 'thr':"T", 'trp':"W", 'tyr':"Y", 'val':"V"}

def writeFeatures(x, corresp, fname):
	ofp = open(fname, "w")
	assert len(x) == len(corresp)
	i = 0
	ofp.write("DEOGEN 1.0 features\nPROVEAN score\tCI\tLOR\tPATH log-odd\tDGR\tREC\tESS\n")
	while i < len(x):
		ofp.write(str(corresp[i][0])+"\t"+str(corresp[i][1])+"\t"+str(x[i])+"\n")
		i+=1
	ofp.close()

def writeResults(y, corresp, fname):
	ofp = open(fname, "w")
	assert len(y) == len(corresp)
	i = 0
	ofp.write("DEOGEN 1.0 predictions\nPROTEIN\tmutation\tscore\tclass (threshold 0.5)\n")
	while i < len(y):
		if y[i] < 0.5:
			cl = "Neutral"
		else:
			cl = "Disease"
		ofp.write(str(corresp[i][0])+"\t"+str(corresp[i][1])+"\t"+str(y[i])+"\t"+str(cl)+"\n")
		i+=1
	ofp.close()

def hash2feats(X):
	x = []
	corresp = []
	for prot in X.items():
		for mut in prot[1].items():
			x.append(mut[1])
			corresp.append((prot[0], mut[0]))
	assert len(x) == len(corresp)
	return x, corresp

def translateMutationPROVEAN(mutation): #gap aware
	#print mutation
	match = re.search(r"^(\w)(\d+)(\w)", mutation)
	wild = mutation[match.start(1):match.end(1)]
	pos = int(mutation[match.start(2):match.end(2)])-1
	mut = mutation[match.start(3):match.end(3)]
	return wild, pos, mut

def getProbMut(msa, pos, mut, freqsTot):	
	if pos > len(msa[0]):
		print "WARNING: position out of the sequence!!!!"
		return [0]
	aaAtPos = []
	#print pos, mut
	for seq in msa:
		if seq[pos] in aleph.values():
			#aaAtPos.append(dUtils.inExt[seq[pos]])
			aaAtPos.append(seq[pos])
	tot = float(len(msa))+20	
	assert len(aleph.values()) == 20
	freqs = {}
	for aa in aleph.values():
		freqs[aa] = (aaAtPos.count(aa)+1)/tot
	assert sum(freqs.values()) -1.0 < 0.1	

	freqMut = freqs[mut]	
	assert freqMut > 0
	CI = computeCI(freqs, freqsTot)	
	return CI

def getProbMutIndel(msa, indel, freqsTot):
	meanCons = 0.0	
	positions = getPositionsInvolved(indel)
	#print positions
	#print len(msa[0])
	for i in positions:
		tmpCons = getProbMut(msa, i, msa[0][i], freqsTot)
		meanCons += tmpCons		
	return meanCons/float(len(positions))

def getPositionsInvolved(indel): #gap aware
	#print indel
	if "_" in indel:
		match = re.search(r"^(\w)(\d+)_(\w)(\d+)([del|ins|delins]{3,6})(\w*)", indel)
		aa1 = indel[match.start(1):match.end(1)]
		pos1 = int(indel[match.start(2):match.end(2)])-1
		aa2 = indel[match.start(3):match.end(3)]
		pos2 = int(indel[match.start(4):match.end(4)])-1
		mode = indel[match.start(5):match.end(5)]
		newAA = indel[match.start(6):match.end(6)]
		return range(pos1, pos2+1)		
	else:
		match = re.search(r"^(\w)(\d+)([del]{3})", indel)		
		aa1 = indel[match.start(1):match.end(1)]
		pos1 = int(indel[match.start(2):match.end(2)])-1
		mode = indel[match.start(3):match.end(3)]		
		return [pos1]

def computeMSAFreqs(msa):
	concatMSA = "".join(msa)
	tot = float(len(concatMSA))+20
	freqsTot = {}
	for aa in aleph.values():
		freqsTot[aa] = (concatMSA.count(aa)+1)/tot
	assert sum(freqsTot.values()) -1.0 < 0.1
	return freqsTot

def computeCI(freqs, freqsTot):
	ci = 0.0
	assert len(aleph.values()) == 20
	for i in aleph.values():			
		ci += (freqs[i] - freqsTot[i])**2
	return math.sqrt(ci)

def readMSA(alName):
	ifp = open(alName, "rb")
	msa = ifp.readlines()
	ifp.close()
	if len(msa) == 0:
		return msa
	i = 0
	while i < len(msa):
		msa[i] = msa[i].strip()
		i += 1	
	return msa

def readProvPreds(provFile):
	ifp = open(provFile, "r")
	lines = ifp.readlines()
	preds = []
	i = 0
	
	while i < len(lines):
		if "#" in lines[i] and "VARIATION" in lines[i] and "SCORE" in lines[i]:
			break
		i+=1
	i+=1
	while i < len(lines):
		tmp = lines[i].strip().split()
		preds.append((tmp[0], tmp[1]))
		i+=1
	return preds

def readInputFileUIDs(fname): 
	'''
	Reads an input file in the following format:
	
	UniprotID	variant
	
	example:
		
	A0AV96	M565V	
	A0AVF1	D310N	
	A0AVI2	I337T
	'''
	variants = {}
	ifp = open(fname)
	lines = ifp.readlines()
	ifp.close()
	a = 0
	for i in lines:
		if len(i) <= 1:
			continue
		a+=1
		tmp = i.strip().split()
		if len(tmp[0]) != 6:
			print "ERROR: the first column of %s should contain UniProt IDs, found %s instead!" % (fname, tmp[0])
			exit(2)
		elif len(tmp) != 2:
			print "ERROR: the input file %s should contain only two columns (UIDs, variant). Found %d columns instead!" % (fname, len(tmp))
			exit(3)
		if not variants.has_key(tmp[0]):
			variants[tmp[0]] = []
		variants[tmp[0]].append(tmp[1])		
	return variants

def writeProvVar(l, name):
	ofp = open(name, "w")
	for i in l:
		ofp.write(i+"\n")
	ofp.close()

def real2real(a):
	if a == ".":
		return -1	
	return float(a)

def str2real(a):
	if a == "E":
		return 1
	if a == "N":
		return -1
	return 0

def selectNFSPfeatures(db, uid):
	#     0			1		2		  3    4    5			6
	#v = [intact, biogrid, consensus, HI, rec, recessive, essential]
	try:
		v = db[uid]		
		#return [real2real(v[2]),real2real(v[4]),str2real(v[6])] #official
		return [real2real(v[2]),real2real(v[4]),str2real(v[6])]
	except:
		return [-1,-1,0]
		#return [-1,-1, 0] #official
		
def filterMSA(alName, si, cov):
		ifp = open(alName, "rb")
		msa = ifp.readlines()
		ifp.close()
		if len(msa) == 0:
			return msa
			
		i = 0
		while i < len(msa):
			msa[i] = msa[i].strip()
			i += 1 
		query = msa.pop(0)
		msaFilt = [query]
		for i in msa:
			if getSeqIDsimilarity(query,i) > si and coverage(query, i) > cov:
				msaFilt.append(i)
		if len(msaFilt)	< 10:
			return msa
		return msaFilt
	
def buildFasta(msa, filename):	
	ofp = open(filename, "w")
	for i in msa:
		ofp.write(i+"\n")
	ofp.close()		
	
def getSeqIDsimilarity(s1,s2): #change it with your favourite similarity measure!
	score = 0
	i = 0 
	while i < min(len(s1), len(s2)):
		if s1[i] == s2[i] and s1[i] != "-":
			score += 1
		i += 1
	return score/float(len(s2)-s2.count("-")) 
	#return score/float(len(s1))
	#return score/float(len(s1))

def coverage(s1, s2):
	return len(s2.replace("-",""))/float(len(s1.replace("-","")))

def readFASTA(seqFile):
	ifp = open(seqFile, "r")
	sl = []
	i = 0
	line = ifp.readline()
	while len(line) != 0:
		if line[0] == '>':			
			sl.append([line.strip().replace("|","-").replace(">","")[:14],""]) #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				sl[i][1] = sl[i][1] + line.strip()
				line = ifp.readline()
			i = i + 1
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!\nShould you try -u option instead of -s?\n")	
	return sl

def readInputFileFasta(fname):
	'''
	Reads an input file in the following fasta-like format:
	
	>unk
	MEEPQSDPSVEPPLSQETFSDLWKLLPENNVLSPLPSQAMDDLMLSPDDIEQWFTEDPGP
	DEAPRMPEAAPPVAPAPAAPTPAAPAPAPSWPLSSSVPSQKTYQGSYGFRLGFLHSGTAK
	SVTCTYSPALNKMFCQLAKTCPVQLWVDSTPPPGTRVRAMAIYKQSQHMTEVVRRCPHHE
	RCSDSDGLAPPQHLIRVEGNLRVEYLDDRNTFRHSVVVPYEPPEVGSDCTTIHYNYMCNS
	SCMGGMNRRPILTIITLEDSSGNLLGRNSFEVRVCACPGRDRRTEEENLRKKGEPHHELP
	PGSTKRALPNNTSSSPQPKKKPLDGEYFTLQIRGRERFEMFRELNEALELKDAQAGKEPG
	GSRAHSSHLKSKKGQSTSRHKKLMFKTEGPDSD

	P72R
	G105C

	>unk2
	MMLSRAKPAVGRGVQHTDKRKKKGRKIPKLEELLSKRDFTGAITLLEFKRHVGEEEEDTN
	LWIGYCAFHLGDYKRALEEYENATKEENCNSEVWVNLACTYFFLGMYKQAEAAGFKASKS
	RLQNRLLFHLAHKFNDEKKLMSFHQNLQDVTEDQLSLASIHYMRSHYQEAIDIYKRILLD
	NREYLALNVYVALCYYKLDYYDVSQEVLAVYLQQIPDSTIALNLKACNHFRLYNGRAAEA
	ELKSLMDNASSSFEFAKELIRHNLVVFRGGEGALQVLPPLVDVIPEARLNLVIYYLRQDD
	VQEAYNLIKDLEPTTPQEYILKGVVNAALGQEMGSRDHMKIAQQFFQLVGGSASECDTIP
	GRQCMASCFFLLKQFDDVLIYLNSFKSYFYNDDIFNFNYAQAKAATGNTSEGEEAFLLIQ
	SEKMKNDYIYLSWLARCYIMNKKPRLAWELYLKMETSGESFSLLQLIANDCYKMGQFYYS
	AKAFDVLERLDPNPEYWEGKRGACVGIFQMIIAGREPKETLREVLHLLRSTGNTQVEYMI
	RIMKKWAKENRVSI

	D310N
	
	>sequence_identifier (non uniprot, if necessary)
	SEQUENCE
	
	variants	
	'''	
	ifp = open(fname, "r")
	sl = {}
	i = 0
	line = ifp.readline()
	while len(line) != 0:
		if line[0] == '>':
			tmpName = line.strip().replace("|","-").replace(">","")[:14]			
			sl[tmpName] = ["",[]] #attention here
			line = ifp.readline()
			while len(line) > 0 and (line[0] != '>' and line[0] != "\n"):
				sl[tmpName][0] += line.strip()
				line = ifp.readline()			
			while len(line) == 0 or line[0] == '\n':				
				line = ifp.readline()	
				
			while len(line) > 0 and line[0] != ">" and line[0] != '\n':	
				if line.strip() != '':
					sl[tmpName][1] += [line.strip()]
				line = ifp.readline()
			while len(line) > 0 and line[0] == '\n':				
				line = ifp.readline()							
			i = i + 1			
		else:
			raise Exception("Syntax error in the fasta file "+fname+"!")	
	
	return sl
	
def writeFASTA(fname, name, seq):
	ofp = open(fname,"w")
	ofp.write(">"+name+"\n")
	ofp.write(seq)
	ofp.close()

def readSTO(fname):
	ifp = open(fname)
	lines = ifp.readlines()
	if len(lines) < 4:
		return None
	tmp = lines[3].strip().split()
	return tmp[1][tmp[1].index("_")+1:tmp[1].index("/")] #not very flexible way to get the uniprot id from uniref100
			
def main():
	print "\n\nPlease don't run me! Run deogen-snv.py instead!\n"
	readSTO("../jhMSAs/unk2.sto")
	return 0

if __name__ == '__main__':
	main()

