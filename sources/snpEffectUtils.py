#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

class SnpEffect:
	def __init__(self,f, uniprotFile):
		self.uniprotDB = readUniprotTab(uniprotFile)
		self.snpEffect = readSNPeffect(f)		
		
	def getScores(self,uid, mut):
		try:
			genes = self.uniprotDB[uid]
			#print genes
		except:
			print "Cannot convert UID %s to genes!" % uid
			return [0,0]
		try:			
			#print self.snpEffect[genes[0]]# snpEffDB = {gene:{mut:[tango, waltz, limbo],mut:[tango, waltz, limbo]}}
			#print "found"
			return self.snpEffect[genes[0]][mut][:2]
		except:
			#print "Mut %s not found!" % mut
			return [0,0]		

def parseGenes(s):
	s = s.strip()
	if "," in s:
		return s.split(",")
	else:
		return s.split(" ")
		
def readUniprotTab(f):
	ifp = open(f,"r")
	uniprotDB = {}
	ifp.readline()
	lines = ifp.readlines()
	for line in lines:
		tmp = line.split("\t")		
		#print tmp
		if not uniprotDB.has_key(tmp[0]):
			uniprotDB[tmp[0]] = []
		uniprotDB[tmp[0]] = parseGenes(tmp[1]) #{UID: [GENE]}
		#raw_input()		
	return uniprotDB

def readSNPeffect(f):
	ifp = open(f,"r")
	snpEffDB = {}
	ifp.readline()
	lines = ifp.readlines()	
	for line in lines:
		tmp = line.split(",")	
		gname = tmp[1].replace("_HUMAN","")
		if not snpEffDB.has_key(gname):
			snpEffDB[gname] = {}
		#print tmp
		snpEffDB[gname][tmp[2]] = [float(tmp[6]),float(tmp[7]),float(tmp[8])]				
	return snpEffDB # snpEffDB = {gene:[tango, waltz, limbo]}

#def selectSNPeffectFeatures(db, prot):
	

def main():
	db = SnpEffect("../databases/snpeffect_export.tab.csv","../databases/uniprot-all.tab")
	print db.snpEffect["STEA2"]
	print "--------"
	print db.getScores("Q8NFT2","F17C")
	return 0

if __name__ == '__main__':
	main()

