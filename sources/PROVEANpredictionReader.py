#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  readPROVEANPredictions.py
#  
#  Copyright 2016 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import cPickle, gc, os
import numpy as np
AA = ["A", "C","D","E","F","G","H","I","K","L","M","N","P","Q","R","S","T","V","W","Y"]

#if not working, try to run extractENSP.py in provean_predictions_data folder and copy the cPickle files

class ProveanPredictionsReader:
	
	def __init__(self, uid2ens, provLookupTable, provScores):
		assert os.path.exists(provScores)
		self.provScores = provScores
		self.lo = cPickle.load(open(provLookupTable))
		self.uid2ens = cPickle.load(open(uid2ens))

	def getProteinPreds(self, uid):	
		try:
			ens = self.uid2ens[uid]
		except:
			return None
		print "   ### %s == %s" % (uid, ens)	
		pos = None
		for i in ens:
			try:
				pos = self.lo[i]
				pid = i
				break
			except:
				print "   #### %s not found" % i
		if pos == None:
			return None	
		ifp = open(self.provScores)
		ifp.seek(pos)
		preds = []	
		line = ifp.readline()
		tmp = line.split("\t")
		assert tmp[0] == pid
		while tmp[0] == pid:				
			#print tmp[0], tmp[1], tmp[2:-1]
			#print parseLine(tmp[2:-1])
			#raw_input()		
			preds.append(self.__parseLine(tmp[2:-1]))
			line = ifp.readline()
			tmp = line.split("\t")		
		return preds 
	
	def getMutPred(self, prot, w, pos, m, tlen):
		preds = self.getProteinPreds(prot)
		if preds == None:
			return None
		if len(preds) != tlen:
			return None
		return (str(w)+str(pos+1)+str(m),preds[pos][m])
		
	def __parseLine(self, l):
		tmp = {}
		assert len(l) == len(AA)
		i = 0
		while i < len(AA):
			tmp[AA[i]] = float(l[i])
			i+=1
		return tmp

def main():
	P = ProveanPredictionsReader( "uid-ens.corresp.cPickle","ExomeProveanPreds_LO.cPickle", "PROVEAN_scores_ensembl66_human.tsv")
	print P.getProteinPreds("P31947")
	print P.getMutPred("P31947","E",0,"D")
	return

if __name__ == '__main__':
	main()
