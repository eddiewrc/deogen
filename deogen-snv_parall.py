#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sources.DRandomForests
import sources.pathwayUtils
import sources.deogenUtils as dUtils
from localConfig import *
import os, sys, cPickle
from multiprocessing import Pool
from sources.PROVEANpredictionReader import ProveanPredictionsReader as PPR

X = {}

def collect_results(results):
	X[results[0]] = results[1]

def main():
	print "************************************************************"
	print "*  Deogen-snv 1.1   (Feb 2016)                             *"
	print "*  Raimondi, Gazzo, Rooman, Lenaerts and Vranken           *"
	print "*                                                          *"
	print "*                                                          *"
	print "************************************************************"
	
	if "-h" in sys.argv[1] or ("-s" not in sys.argv[1] and "-u" not in sys.argv[1]):
		print "\n\nUSAGE: deogenSNVs.py [OPTIONS]"
		print "\t-u\tproteins identified by Uniprot Id in the input file"
		print "\t-s\tprotein indentified by sequence in the input file"
		exit(1)
			
	inFile = sys.argv[2]
	if not os.path.exists(inFile):
		print "ERROR: the specified input file %s doesn't exist! "%inFile
		exit(3)
	print "Creating folders if needed...",
	os.system("mkdir -p "+WORKING_DIR+" "+DOWNLOAD_DIR+" "+JH_MSA_FOLDER )
	print "Done."		
	print "Check PROVEAN installation...",
	t = os.system(PROVEAN_SH+" --help  >> /dev/null")
	if t != 0:
		print "\nERROR: "+ PROVEAN_SH + " is not present or not working. Please set the correct path in localConfig.py and install PROVEAN. (see README)\n"
		exit(4)
	print "Done."	
	print "Reading REC, ESS and DGR annotations...",
	nfsp = cPickle.load(open("DATA/dbNFSP.cPickle"))
	assert nfsp != None and nfsp.keys() > 1
	print "Done."
	print "Reading pathway log-odd scores...",
	P = cPickle.load(open("DATA/pathway.cPickle"))
	assert P != None	
	print "Done."
	print "Reading RandomForest trained model...",
	model = cPickle.load(open("DATA/modelSNVs.cPickle"))
	assert model != None
	print "Done."
	os.system("mkdir -p "+TMP_DIR)
	print "Checking precomputed PROVEAN predictions..."
	Preader = None
	USE_PRECOMPUTED = False #sorry but I found a bug in the conversion from ENSP ids to uniprot accessions numbers! disabled for now!
	if os.path.exists(PRECOMPUTED_PROVEAN_PREDS_FOLDER+"/PROVEAN_scores_ensembl66_human.tsv") and USE_PRECOMPUTED == True:
		print "Found! Initializing reader..."
		Preader = PPR("DATA/uid-ens.corresp.cPickle", "DATA/ExomeProveanPreds_LO.cPickle", PRECOMPUTED_PROVEAN_PREDS_FOLDER+"/PROVEAN_scores_ensembl66_human.tsv")
		print "Done."
	else:
		print "Precomputed PROVEAN scores not found!"
	################## READING input variants!	
	
	print "Reading Uniprot IDs-based input file %s..." %(inFile),
	sys.stdout.flush()
	varList = dUtils.readInputFileUIDs(inFile)
	#print varList["Q8NCM8"]
	#raw_input()
	print "Done."		
	print "\nRetrieved %d variants on %d proteins. Starting Deogen!" % (len(varList.items()), len(varList.keys())) #fix this!
	sys.stdout.flush()
	totVar = len(varList.items())
	#MAKING PREDICTIONS IN PARALLEL!
	pool = Pool(CORES)
	print "Starting parallelization..."
	for prot in varList.items():
		pool.apply_async(execution, args=(prot, P, nfsp, Preader, varList), callback=collect_results )
		
	pool.close()
	pool.join()
	print "Computations finished!"
	# FEATURE COLLECTION FINISHED. PREDICTION TIME!
	#print X
	print "Computing DEOGEN predictions...",
	sys.stdout.flush()
	#x, corresp = dUtils.list2feats(results)
	x, corresp = dUtils.hash2feats(X)
	y = model.decision_function(x)
	
	dUtils.writeResults(y, corresp, inFile+".deogen.predictions")
	dUtils.writeFeatures(x, corresp, inFile+".deogen.features")
	cPickle.dump(X, open(inFile+".deogen.features.cPickle","w")
	print "Done."
	print "Cleaning up...",
	os.system("rm -r " + TMP_DIR )
	print "Done."
	print "\nE infine uscimmo a riveder le stelle.",
	sys.stdout.flush()		
	return 0

def execution(prot, P, nfsp, Preader, varList):
	X = {}	
	print " > Working on protein %s" % prot[0]
	print "  >> Downloading sequence from UniProt..."
	sys.stdout.flush()
	if not os.path.exists(DOWNLOAD_DIR+prot[0]+".fasta"):
		t = os.system("wget -t "+str(RETRY)+" --retry-connrefused --directory-prefix="+DOWNLOAD_DIR+" http://www.uniprot.org/uniprot/"+prot[0]+".fasta > /dev/null")
		if t != 0:
			print "\nERROR: Problem during downloading %s fasta file from Uniprot. Skipping!" % (mut)
			sys.stdout.flush()
			return "Download error "+prot[0]			
	
	print "  >> Check variant consistency...",
	tseq = dUtils.readFASTA(DOWNLOAD_DIR+prot[0]+".fasta")[0][1]
	#print tseq	
	tmp = []	
	alreadyDone = set()	
	for mut in prot[1]:							
		print mut, prot[0]
		w, pos, m = dUtils.translateMutationPROVEAN(mut)	
		if Preader != None:
			ptmp = Preader.getMutPred(prot[0], w, pos, m, len(tseq))
			if ptmp != None:
				print "Prediction already computed found in PROVEAN exome db!"
				tmp.append(ptmp)
				alreadyDone.add(ptmp[0])
				continue							
		if w != tseq[pos]:
			print "\nWARNING: Reference AA does not match: %s in uniprot sequence, but %s is provided at position %d" % (tseq[pos], w, pos)
			print "WARNING: DEOGEN will replace %s with %s as wildtype AA! New variant: %s%d%s\n" % (w, tseq[pos], tseq[pos], pos, m)
			vtmp = varList[prot[0]]
			vtmp.remove(mut)
			vtmp.append("%s%d%s" % (tseq[pos], pos+1, m))
			varList[prot[0]] = vtmp	
				
	todoVars = list(set(prot[1]).difference(alreadyDone))
	#print tmp
	#print alreadyDone
	#print "todo"
	#print todoVars
	if len(todoVars) > 0:				
		print "Done.\n  >> Starting PROVEAN... (it may take some minutes) ",
		sys.stdout.flush()
		dUtils.writeProvVar(todoVars, WORKING_DIR+prot[0]+".var")	
		if not os.path.exists(WORKING_DIR+prot[0]+".prov") or os.stat(WORKING_DIR+prot[0]+".prov").st_size == 0 or -1 == dUtils.readProvPreds(WORKING_DIR+prot[0]+".prov"):
			if SAVE_SUPPORTING:
				if os.path.exists(WORKING_DIR+prot[0]+".support"):
					os.system(PROVEAN_SH+" -q "+DOWNLOAD_DIR+prot[0]+".fasta -v "+WORKING_DIR+prot[0]+".var --supporting_set "+WORKING_DIR+prot[0]+".support"+" --num_threads 1 --tmp_dir "+ TMP_DIR +" > "+WORKING_DIR+prot[0]+".prov")
				else:	
					os.system(PROVEAN_SH+" -q "+DOWNLOAD_DIR+prot[0]+".fasta -v "+WORKING_DIR+prot[0]+".var --save_supporting_set "+WORKING_DIR+prot[0]+".support"+" --num_threads 1 --tmp_dir "+ TMP_DIR +" > "+WORKING_DIR+prot[0]+".prov")
			else:
				os.system(PROVEAN_SH+" -q "+DOWNLOAD_DIR+prot[0]+".fasta -v "+WORKING_DIR+prot[0]+".var --num_threads 1 --tmp_dir "+ TMP_DIR +" > "+WORKING_DIR+prot[0]+".prov")				
			print "Done."
		else:
			print "PROVEAN predictions already computed!"
		sys.stdout.flush()
		tmp +=  dUtils.readProvPreds(WORKING_DIR+prot[0]+".prov")
		if tmp == -1:
			print " ERROR: PROVEAN experienced some problems, no predictions!!!"
			return -1
	if not X.has_key(prot[0]):
		X[prot[0]] = {}
	for i in tmp:
		X[prot[0]][i[0]] = [float(i[1])]
	
	#retrieving JH MSAs ###############################
	
	sys.stdout.flush()	
		
	if not os.path.exists(JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX) or os.stat(JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX).st_size == 0:
		print "  >> Building JackHmmer alignment...",
		sys.stdout.flush()	
		t = os.system(JH_BIN+" --acc --notextw --noali -E "+str(JH_EVALUE)+" -N "+str(JH_ITER)+" --cpu 1 -A "+JH_MSA_FOLDER+prot[0]+".sto "+DOWNLOAD_DIR+prot[0]+".fasta"+" "+JH_DATABASE+" > /dev/null")
		if t != 0:
			print "\nERROR: jackHmmer encountered an error!"
			sys.stdout.flush()
			exit(5)
		print "Done."
		sys.stdout.flush()
		print "  >> Reformatting and reading JackHmmer alignment..."
		sys.stdout.flush()
		os.system("./sources/reformat.pl sto fas "+JH_MSA_FOLDER+prot[0]+".sto "+" "+JH_MSA_FOLDER+prot[0]+".fas > /dev/null")
		os.system("./sources/trim.py "+JH_MSA_FOLDER+prot[0]+".fas"+" $(head -n 1 "+JH_MSA_FOLDER+prot[0]+".fas"+" | cut -f2 -d \">\") > "+JH_MSA_FOLDER+prot[0]+".hmmer")	
		filteredMsa = dUtils.filterMSA(JH_MSA_FOLDER+prot[0]+".hmmer", SI, COV)
		dUtils.buildFasta(filteredMsa, JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX)
		print "  >> Cleaning MSAs...",
		os.system("rm "+JH_MSA_FOLDER+prot[0]+".sto "+JH_MSA_FOLDER+prot[0]+".fas "+JH_MSA_FOLDER+prot[0]+".hmmer")
		print "Done."
	else:
		print "  >> JH alignment already built and filtered!"
	#os.system("rm "+i[0] +" "+ fileName+"HMMERAlignDir/"+alignName+".sto " +" "+ fileName+"HMMERAlignDir/"+alignName+".fas " )
		
	print "  >> Collecting CI and LOR features..."
	sys.stdout.flush()			
	if os.path.exists(JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX):
		msa = dUtils.readMSA(JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX)
	else:
		print "\n ERROR: %s MSA is not present!" % (JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX)
		exit(6)
	if len(msa) == 0:
		print "\nWARNING: CI and LOR predictions for %s not available due to empty MSA. Skipping!\n" % prot[0]
		del X[prot[0]]
		return "Empty msa error "+prot[0]
	else:
		print "  >> Read %d sequences." % len(msa)
		
	freqsTot = dUtils.computeMSAFreqs(msa)
	for mut in prot[1]:				
		w, pos, m = dUtils.translateMutationPROVEAN(mut)				
		CI, prob = dUtils.getProbMut(msa, pos, (w,m), freqsTot)								
		X[prot[0]][mut] += [CI, prob]
	
	print "  >> Done."
	sys.stdout.flush()			
	
	print "  >> Collecting pathway log-odd scores...",
	sys.stdout.flush()
	
	tmpP = [P.getLogOdd(prot[0])]
	for mut in prot[1]:
		X[prot[0]][mut] += tmpP
	
	print "Done."
	sys.stdout.flush()
	
	print "  >> Collecting DGR, REC and ESS features...",
	sys.stdout.flush()			
	
	for mut in prot[1]:
		X[prot[0]][mut] += dUtils.selectNFSPfeatures(nfsp, prot[0])
		
	print "Done.\n"
	sys.stdout.flush()
	return (prot[0], X[prot[0]])


if __name__ == '__main__':
	main()

